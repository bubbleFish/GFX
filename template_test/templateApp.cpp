/*

Book:      	Game and Graphics Programming for iOS and Android with OpenGL(R) ES 2.0
Author:    	Romain Marucchi-Foino
ISBN-10: 	1119975913
ISBN-13: 	978-1119975915
Publisher: 	John Wiley & Sons	

Copyright (C) 2011 Romain Marucchi-Foino

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use of
this software. Permission is granted to anyone who either own or purchase a copy of
the book specified above, to use this software for any purpose, including commercial
applications subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim that
you wrote the original software. If you use this software in a product, an acknowledgment
in the product would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be misrepresented
as being the original software.

3. This notice may not be removed or altered from any source distribution.

*/

#include "templateApp.h"

/* The main structure of the template. This is a pure C struct, you initialize the structure
   as demonstrated below. Depending on the type of your type of app simply comment / uncomment
   which event callback you want to use. */

TEMPLATEAPP templateApp = {
							/* Will be called once when the program start. */
							templateAppInit,
							
							/* Will be called every frame. This is the best location to plug your drawing. */
							templateAppDraw,
							
							/* This function will be triggered when a new touche is recorded on screen. */
							//templateAppToucheBegan,
							
							/* This function will be triggered when an existing touche is moved on screen. */
							//templateAppToucheMoved,
							
							/* This function will be triggered when an existing touche is released from the the screen. */
							//templateAppToucheEnded,
							
							/* This function will be called everytime the accelerometer values are refreshed. Please take
							not that the accelerometer can only work on a real device, and not on the simulator. In addition
							you will have to turn ON the accelerometer functionality to be able to use it. This will be
							demonstrated in the book later on. */
							//templateAppAccelerometer // Turned off by default.
						  };

#define VERTEX_SHADER (char*)"vertex.vert"
#define FRAGMENT_SHADER (char*)"fragment.frag"

#define DEBUG_SHADERS 1

PROGRAM *program = NULL;
MEMORY *memory = NULL;

void programInit()
{
    program = PROGRAM_init((char*)"default");
    program->vertex_shader = SHADER_init(VERTEX_SHADER, GL_VERTEX_SHADER);
    program->fragment_shader = SHADER_init(FRAGMENT_SHADER, GL_FRAGMENT_SHADER);
    memory = mopen(VERTEX_SHADER, 1);
    if (memory) {
        if (!SHADER_compile(program->vertex_shader, (char*)memory->buffer, DEBUG_SHADERS)) {
            exit(1);
        }
    }
    memory = mclose(memory);
    
    memory = mopen(FRAGMENT_SHADER, 1);
    if (memory) {
        if (!SHADER_compile(program->fragment_shader, (char*)memory->buffer, DEBUG_SHADERS)) {
            exit(2);
        }
    }
    memory = mclose(memory);
    
    if (! PROGRAM_link(program, DEBUG_SHADERS)) {
        exit(3);
    }
}

//添加第三个维度
static const float POSITION[12] = {
    -0.5f,0.f,-0.5f,
    0.5f,0.f,-0.5f,
    -0.5f,0.f,0.5f,
    0.5f,0.f,0.5f
};

static const float COLOR[16] = {
    1.f,0.f,0.f,1.f,
    0.f,1.f,0.f,1.f,
    0.f,0.f,1.f,1.f,
    1.f,1.f,0.f,1.f
};


void templateAppInit( int width, int height )
{
    console_print("templateAppInit, screen size: %d x %d\n",width,height);
	// Setup the exit callback function.
	atexit( templateAppExit );
	
	// Initialize GLES.
	GFX_start();
	
	// Setup a GLES viewport using the current width and height of the screen.
	glViewport( 0, 0, width, height );
	
    GFX_set_matrix_mode(PROJECTION_MATRIX);
    GFX_load_identity();
    /*
        正射2D投影
        GFX_set_orthographic_2d
        正交投影
        GFX_set_orthographic 当Y位置超过100后,将被远端剪切面裁剪
        透视投影
        GFX_set_perspective fov 参数视场,角度越大,视角范围越宽广;角度越小,投影也越窄
    */
    GFX_set_perspective(45.f, (float)width/height, 0.01f, 100.f, 0.f);
    //不要剪切背面
    glDisable(GL_CULL_FACE);
    
    programInit();
    
    GFX_set_matrix_mode(MODELVIEW_MATRIX);
    GFX_load_identity();
    
    /*
     e 摄像机世界座标系的位置
     c 摄像机正在看的世界座标系
     */
    vec3 e = {0.0f,-3.0f,0.f}, c = {0.f,0.f,0.f}, u = {0.f,0.f,1.f};
    GFX_look_at(&e, &c, &u);
    
    GFX_rotate(50.f, 0.f, 1.f, 0.f);
}

void templateAppDraw( void )
{
	// Clear the depth, stencil and colorbuffer.
    glClearColor(0.5f, 0.5f, 0.5f, 1.f);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	/* Insert your drawing code here */
    
    if (program->pid) {
        glUseProgram(program->pid);
        
        char attribute,uniform;
        uniform = PROGRAM_get_uniform_location(program, (char*)"MODELVIEWPROJECTIONMATRIX");
        glUniformMatrix4fv(uniform, 1, GL_FALSE, (float*)GFX_get_modelview_projection_matrix());
        
        attribute = PROGRAM_get_vertex_attrib_location(program, (char*)"POSITION");
        glEnableVertexAttribArray(attribute);
        glVertexAttribPointer(attribute, 3, GL_FLOAT, GL_FALSE, 0 ,POSITION);
        
        attribute = PROGRAM_get_vertex_attrib_location(program, (char*)"COLOR");
        glEnableVertexAttribArray(attribute);
        glVertexAttribPointer(attribute, 4, GL_FLOAT, GL_FALSE, 0 ,COLOR);
        
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        
        GFX_error();
    }
}

void templateAppExit( void )
{
    console_print("templateAppExit... \n");
    /* Code to run when the application exit, perfect location to free everything. */
    if (memory) {
        memory = mclose(memory);
    }
    
    if (program && program->vertex_shader) {
        program->vertex_shader = SHADER_free(program->vertex_shader);
    }
    if (program && program->fragment_shader) {
        program->fragment_shader = SHADER_free(program->fragment_shader);
    }
    if (program) {
        program = PROGRAM_free(program);
    }
}

void templateAppToucheBegan( float x, float y, unsigned int tap_count )
{
	/* Insert code to execute when a new touche is detected on screen. */
}


void templateAppToucheMoved( float x, float y, unsigned int tap_count )
{
	/* Insert code to execute when a touche move on screen. */
}


void templateAppToucheEnded( float x, float y, unsigned int tap_count )
{
	/* Insert code to execute when a touche is removed from the screen. */
}


void templateAppAccelerometer( float x, float y, float z )
{
	/* Insert code to execute with the accelerometer values ( when available on the system ). */
}



